/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transito;

import ModelosUI.TransitoUI;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyectotransito.Categoria;
import proyectotransito.Licencia;
import proyectotransito.MotivoMulta;
import proyectotransito.OrganismoTransito;
import proyectotransito.Persona;
import proyectotransito.Vehiculo;

/**
 *
 * @author gusad
 */
public class ProyectoTransito {
    
    public static void main(String[] args) throws Exception {
        
        OrganismoTransito org = new OrganismoTransito("Aaaa");
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaExpedicion = sdf.parse("01/01/2016");
        Date fechaVencimiento = sdf.parse("01/01/2017");
        
        short modelo = 2012;
        org.agregarLicencia(new Licencia(Categoria.B2, new Persona(111222333, "Samuel", "Burbano"), fechaExpedicion, fechaVencimiento));
        org.agregarVehiculo(new Vehiculo("142a", modelo, "rojo", "mazda"));        
        short codigo = 1000; 
        org.agregarMotivoMulta(new MotivoMulta(codigo, "Pasar Semarofo en rojo",20000));
        new TransitoUI(org).setVisible(true);
        
    }
    
}
//        OrganismoTransito org = new OrganismoTransito();
//        short modelo = 2012;
//        org.agregarAgente(new Agente(1113685637, "Gustavo", "Quintero", 0426));
//        org.agregarVehiculo(new Vehiculo("142a", modelo, "rojo", "mazda"));
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//        Date fechaExpedicion = sdf.parse("2016/01/01");
//        Date fechaVencimiento = sdf.parse("2018/01/01");
//
//        org.agregarLicencia(new Licencia(Categoria.B2, new Persona(111222333, "Samuel", "Burbano"), fechaExpedicion, fechaVencimiento));
//        TransitoUI transitoUI = new TransitoUI(org);
//        transitoUI.setVisible(true);
//        ObjectInputStream stream = null;
//        OrganismoTransito transito = null;
//        try {
//            stream = new ObjectInputStream(new FileInputStream("data.obj"));
//            transito = (OrganismoTransito) stream.readObject();
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(ProyectoTransito.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(ProyectoTransito.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (ClassNotFoundException ex) {
//            Logger.getLogger(ProyectoTransito.class.getName()).log(Level.SEVERE, null, ex);
//  
//        } finally {
//            try {
//                if (stream != null) {
//                    stream.close();
//                }
//            } catch (IOException ex) {
//                Logger.getLogger(ProyectoTransito.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        if (transito == null) {
//            
//        }

//Categoria cat = Categoria.A1;
//System.out.println(cat.name());
//Persona per = new Persona(1113, "Gustavo", "Adolfo");
//System.out.println("La persona es: " + per.getNombre());    
//Agente agen1 = new Agente(1123, "gustavo","quinte", 12);
//OrganismoTransito OT = new OrganismoTransito();
//        OT.agregarAgente(new Agente(1113,"gustavo","Quintero",0426));
//        OT.agregarAgente(new Agente(1112,"gustavo","Quintero",0426));
//        OT.agregarAgente(new Agente(1113,"raro","asda",0426));
//        OT.mostrarAgente();

