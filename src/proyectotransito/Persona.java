/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectotransito;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author gusad
 */
@Entity
public class Persona implements Serializable {

    @Id
    private long identificacion;
    private String nombre;
    private String apellido;

    public Persona(long identificacion, String nombre, String apellido) throws Exception {
        if (identificacion < 0) {
            throw new Exception("El nùmero de identificacion no puede ser menor que 0");
        }
        if (nombre == null || "".equals(nombre.trim())) {
            throw new Exception("nombre no puede ser nulo/vacio");
        }
        if (apellido == null || "".equals(apellido.trim())) {
            throw new Exception("El Apellido no puede ser nulo/vacio");
        }
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public Persona() {
    }

    public long getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(long identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

}
