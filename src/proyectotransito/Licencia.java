/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectotransito;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author salasistemas
 */
public class Licencia implements Serializable {

    private final Categoria categoria;
    private Persona persona;
    private Date fechaExpedicion;
    private Date fechaVencimiento;

    public Licencia(Categoria categoria, Persona persona, Date fechaExpedicion, Date fechaVencimiento) throws Exception {
        if (fechaVencimiento.before(fechaExpedicion)) {
            throw new Exception("La fecha de vencimiento no puede ser anterior a la fecha de expediciòn");
        }
        this.categoria = categoria;
        this.persona = persona;
        this.fechaExpedicion = fechaExpedicion;
        this.fechaVencimiento = fechaVencimiento;
    }

    public Licencia(Categoria categoria) {
        this.categoria = categoria;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Date getFechaExpedicion() {
        return fechaExpedicion;
    }

    public void setFechaExpedicion(Date fechaExpedicion) {
        this.fechaExpedicion = fechaExpedicion;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

}
