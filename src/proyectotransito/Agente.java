/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectotransito;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author salasistemas
 */
@Entity
@NamedQueries(
        @NamedQuery(name = "buscarPorPlaca", query
                = "SELECT a FROM Agente a WHERE a.numeroPlaca = :placa"))
public class Agente extends Persona implements Serializable {

    @Column(nullable = false, unique = true)
    private int numeroPlaca;

    public Agente() {
    }

    public Agente(long identificacion, String nombre, String apellido, int numeroPlaca) throws Exception {
        super(identificacion, nombre, apellido);
        if (numeroPlaca < 0) {
            throw new Exception("El número de la placa debe ser mayor a 0");
        }
        this.numeroPlaca = numeroPlaca;
    }

    public int getNumeroPlaca() {
        return numeroPlaca;
    }

    public void setNumeroPlaca(int numeroPlaca) {
        this.numeroPlaca = numeroPlaca;
    }

}
