/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectotransito;

import java.io.Serializable;

/**
 *
 * @author gusad
 */
public class Vehiculo implements Serializable {

    private String placa;
    private short modelo;
    private String color;
    private String marca;

    public Vehiculo(String placa, short modelo, String color, String marca) throws Exception {
        if (placa == null || "".equals(placa.trim())) {
            throw new Exception("La placa no puede ser nulo/vacio");
        }
        if (placa.length() < 6 && placa.length() > 6) {
            throw new Exception("La placa no puede ser menor a 6 digitos");
        }
        if (modelo < 1800) {
            throw new Exception("El modelo no puede ser menor que 1800");
        }
        if (color == null || "".equals(color.trim())) {
            throw new Exception("El color no puede ser nulo/vacio");
        }
        if (marca == null || "".equals(marca.trim())) {
            throw new Exception("La marca no puede ser nulo/vacio");
        }
        this.placa = placa;
        this.modelo = modelo;
        this.color = color;
        this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public short getModelo() {
        return modelo;
    }

    public void setModelo(short modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

}
