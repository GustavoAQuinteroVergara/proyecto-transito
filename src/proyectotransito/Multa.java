/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectotransito;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author gusad
 */
@Entity
public class Multa implements Serializable {

    @Id
    @GeneratedValue
    private long pk;
    private int valor;
    private Vehiculo vehiculo;
    private Persona conductor;
    private Date fechaImpuesta;
    private Agente agente;
    private final List<MotivoMulta> motivosMultas = new LinkedList<>();

    public int getValor() {
        return valor;
    }

    public Multa(int valor, Vehiculo vehiculo, Date date) throws Exception {
        if (valor < 0) {
            throw new Exception("El valor no puede ser menor a 0");
        }
        this.valor = valor;
        this.vehiculo = vehiculo;

    }

    public Persona getPersona() {
        return conductor;
    }

    public void setPersona(Persona conductor) {
        this.conductor = conductor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public List<MotivoMulta> getMotivosMultas() {
        return motivosMultas;
    }

    public Persona getConductor() {
        return conductor;
    }

    public void setConductor(Persona conductor) {
        this.conductor = conductor;
    }

    public Date getFechaImpuesta() {
        return fechaImpuesta;
    }

    public void setFechaImpuesta(Date fechaImpuesta) {
        this.fechaImpuesta = fechaImpuesta;
    }

    public Agente getAgente() {
        return agente;
    }

    public void setAgente(Agente agente) {
        this.agente = agente;
    }

}
