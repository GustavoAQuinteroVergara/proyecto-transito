/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectotransito;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author gusad
 */
@Entity

public class MotivoMulta implements Serializable {

    @Id
    private short codigo;
    private String descripcion;
    private int valor;

    public MotivoMulta(short codigo, String descripcion, int valor) throws Exception {
        if (codigo < 0) {
            throw new Exception("El còdigo no puede ser menor que 0");
        }
        if (descripcion == null || "".equals(descripcion.trim())) {
            throw new Exception("La descripcion no puede ser vacia/nula");
        }
        if (valor < 0) {
            throw new Exception("El valor no puede ser menor que 0");
        }
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.valor = valor;
    }

    public MotivoMulta() {
    }

    public short getCodigo() {
        return codigo;
    }

    public void setCodigo(short codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

}
