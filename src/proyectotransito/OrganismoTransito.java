package proyectotransito;

import TransitoPersistencia.AgenteJpaController;
import TransitoPersistencia.MotivoMultaJpaController;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author salasistemas
 */
public class OrganismoTransito  implements Serializable{
    
    private String ciudad;
    private final List<Agente> agentes = new LinkedList<>();
    private final List<MotivoMulta> motivosMultas = new LinkedList<>();
    private final List<Vehiculo> vehiculos = new LinkedList<>();
    private final List<Licencia> licencias = new LinkedList<>();
    private final List<Multa> multas = new LinkedList<>();
    
    private MotivoMultaJpaController motivoMultaJpaController ;
    private AgenteJpaController agenteJpaController ;
    
    public OrganismoTransito(String ciudad) {
        this.ciudad = ciudad;
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ProyectoTransito_Quintero_GustavoPU");
        motivoMultaJpaController = new MotivoMultaJpaController(emf);
        agenteJpaController = new AgenteJpaController(emf);
    }
    

    public List<Agente> getAgentes() {
        //return agentes;
        return agenteJpaController.findAgenteEntities();
    }

    public List<MotivoMulta> getMotivosMultas() {
        return motivosMultas;
        //return motivoMultaJpaController.findMotivoMultaEntities();
    }

    public List<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public List<Licencia> getLicencia() {
        return licencias;
    }

    public List<Multa> getMultas() {
        return multas;
    }

    public void agregarAgente(Agente agente) throws Exception {
//        for (int i = 0; i < agentes.size(); i++) {
//            if (agentes.get(i).getIdentificacion() == agente.getIdentificacion()) {
//                throw new Exception("Ya existe un agente con esta identificación");
//            }
//
//        }
//        agentes.add(agente);
        agenteJpaController.create(agente);
    }

    public void agregarVehiculo(Vehiculo vehiculo) throws Exception {
        for (int i = 0; i < vehiculos.size(); i++) {
            if (vehiculos.get(i).getPlaca().equals(vehiculo.getPlaca())) {
                throw new Exception("Ya existe un vehiculo registrado con esta placa");
            }

        }
        vehiculos.add(vehiculo);
    }

    public void agregarMotivoMulta(MotivoMulta motivoMulta) throws Exception {
        for(int i = 0; i < motivosMultas.size(); i++){
            if(motivosMultas.get(i).getCodigo() == motivoMulta.getCodigo()){
                throw new Exception ("Ya existe una multa con este código");
            }
        }
        motivosMultas.add(motivoMulta);
    }

    public void agregarLicencia(Licencia licencia) throws Exception {
        for (int i = 0; i < licencias.size(); i++) {
            if (licencias.get(i).getPersona().getIdentificacion() == licencia.getPersona().getIdentificacion()) {
                throw new Exception("Ya existe una persona registrada con esta identificación");
            }

        }
        licencias.add(licencia);
    }

    public void agregarMultas(Multa multa) {
        multas.add(multa);
    }

    public void mostrarAgente() {
        for (int i = 0; i < agentes.size(); i++) {
            System.out.println(agentes.get(i).getIdentificacion() + " " + agentes.get(i).getNombre() + " " + agentes.get(i).getApellido() + " " + agentes.get(i).getNumeroPlaca());
        }
    }

    public void mostrarVehiculo() {
        for (int i = 0; i < vehiculos.size(); i++) {
            System.out.println(vehiculos.get(i).getPlaca() + " "
                    + vehiculos.get(i).getColor() + " " + vehiculos.get(i).getMarca());
        }
    }

    public void motivoMulta() {
        for (int i = 0; i < motivosMultas.size(); i++) {
            System.out.println(motivosMultas.get(i).getCodigo() + " "
                    + motivosMultas.get(i).getDescripcion() + " " + motivosMultas.get(i).getValor());
        }
    }

    public void mostrarLicencia() {
        for (int i = 0; i < licencias.size(); i++) {
            System.out.println(licencias.get(i).getFechaExpedicion() + " "
                    + licencias.get(i).getFechaVencimiento());
        }
    }

    public void mostrarMultas() {
        for (int i = 0; i < multas.size(); i++) {
            System.out.println(multas.get(i).getValor());
        }
    }

    public Vehiculo buscarVehiculo(String placa) {
        Vehiculo veh = null;
        for (int i = 0; i < vehiculos.size(); i++) {
            if (placa.equals(vehiculos.get(i).getPlaca())) {
                veh = vehiculos.get(i);
            }
        }
        return veh;
    }

    public Agente buscarAgente(int numeroPlaca) {
//        Agente agen = null;
//        for (int i = 0; i < agentes.size(); i++) {
//            if (numeroPlaca == agentes.get(i).getNumeroPlaca()) {
//                agen = agentes.get(i);
//            }
//        }
//        return agen;
        return agenteJpaController.buscarPorPlaca(numeroPlaca);
    }
    
    public  LinkedList<Licencia> buscarLicencias(long identificacion) throws Exception {
    
        LinkedList<Licencia> encontradas = new LinkedList<>();
        for(Licencia licencia : this.licencias){
            if(licencia.getPersona().getIdentificacion() == identificacion){
                encontradas.add(licencia);
            }
        }
        return encontradas;
    }
    
    public MotivoMulta buscarMotivoMulta(short codigo) throws Exception {
//        for(MotivoMulta motivoMulta : this.motivosMultas){
//            if(motivoMulta.getCodigo()== codigo){
//                return motivoMulta;
//            }
//        }
        //return motivoMultaJpaController.findMotivoMulta(codigo);
        MotivoMulta motivo = null;
        for(int i = 0; i < motivosMultas.size(); i++){
        if (codigo == motivosMultas.get(i).getCodigo()) {
                motivo = motivosMultas.get(i);
            }
        }
        return motivo;
    }
    
   
}



